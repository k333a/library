﻿
using System.Collections.ObjectModel;

namespace MyLibTask
{
    public class LibSearch : ISearch
    {
        public ObservableCollection<T> Find<T>(ObservableCollection<T> list, string data)
        {
            ObservableCollection<T> foundItemList = new ObservableCollection<T>();
            foreach (T item in list)
            {
                foreach (var property in typeof(T).GetProperties())
                {
                    if (property.GetValue(item).ToString() == data)
                        foundItemList.Add(item);
                }
            }
            return foundItemList;
        }
    }
}
