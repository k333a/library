﻿using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace MyLibTask
{
    public class DbManager : IDbManager
    {

        //public void ChangeSubscriberHandler(ObservableCollection<Book> list, string DbName)
        //{
        //    DBsubWriter = WriteToDb;

        //public void WriteToDb<T>(List<T> list, string DbName)
        //{
        //    DataContractSerializer formatter = new DataContractSerializer(typeof(List<T>));
        //    using (FileStream fs = new FileStream(DbName, FileMode.Create))
        //    {
        //        formatter.WriteObject(fs, list);
        //    }
        //}


        //public List<T> ReadFromDb<T>(string DbName)
        //{
        //    DataContractSerializer formatter = new DataContractSerializer(typeof(List<T>));
        //    using (FileStream fs = new FileStream(DbName, FileMode.OpenOrCreate))
        //    {

        //        List<T> list = (List<T>)formatter.ReadObject(fs);
        //        return list;
        //    }
        //}

        public void WriteToDb<T>(ObservableCollection<T> list, string DbName)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ObservableCollection<T>));
            using (FileStream fs = new FileStream(DbName, FileMode.Create))
            {
                formatter.Serialize(fs, list);
            }
        }


        public ObservableCollection<T> ReadFromDb<T>(string DbName)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(ObservableCollection<T>));
            using (FileStream fs = new FileStream(DbName, FileMode.OpenOrCreate))
            {

                ObservableCollection<T> list = (ObservableCollection<T>)formatter.Deserialize(fs);
                return list;

            }
        }

        public ObservableCollection<T> CheckDB<T>(string DBLibrary, ObservableCollection<T> books)
        {
            if (File.Exists(DBLibrary))
            {
                books = ReadFromDb<T>(DBLibrary);
            }
            return books;
        }
    }
}

