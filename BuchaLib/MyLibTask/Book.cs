﻿using System;
using System.Runtime.Serialization;

namespace MyLibTask
{
    public enum BookType
    {
        Historical,
        Science,
        Children,
        Art
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public int PagesAmount { get; set; }
        [DataMember]
        public BookType TypeOfBook { get; set; }
        [DataMember]
        public bool Available { get; set; } = true;

        public Book() { }

        public Book(string name, string author, int pageAmount, BookType typeOfBook)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Author = author;
            PagesAmount = pageAmount;
            TypeOfBook = typeOfBook;
        }

        public override string ToString()
        {
            return Id + " " + Name + " " + Author + " " + TypeOfBook + " " + Available;
        }
    }
}
