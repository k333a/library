﻿using System.Collections.ObjectModel;

namespace MyLibTask
{
    public class ArchiveManager : IArchiveManager
    {

        public ObservableCollection<T> AddItem<T>(T item, ObservableCollection<T> itemList)
        {
            itemList.Add(item);
            return itemList;
        }

        public ObservableCollection<T> RemoveItem<T>(T item, ObservableCollection<T> itemList)
        {
            itemList.Remove(item);
            return itemList;
        }
    }
}
