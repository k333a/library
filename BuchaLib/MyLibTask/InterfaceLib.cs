﻿using System.Collections.ObjectModel;

namespace MyLibTask
{

    public interface IArchiveManager
    {
        ObservableCollection<T> AddItem<T>(T item, ObservableCollection<T> itemList);
        ObservableCollection<T> RemoveItem<T>(T item, ObservableCollection<T> itemList);
    }

    public interface IDbManager
    {
        void WriteToDb<T>(ObservableCollection<T> list, string DbName);
        ObservableCollection<T> ReadFromDb<T>(string DbName);
        ObservableCollection<T> CheckDB<T>(string DBLibrary, ObservableCollection<T> books);
    }

    public interface ISearch
    {
        ObservableCollection<T> Find<T>(ObservableCollection<T> list, string data);
    }

    public interface ILibrary
    {
        ObservableCollection<Book> AddBook(Book book);
        ObservableCollection<Book> RemoveBook(Book book);
    }

    public interface ISubscribeService
    {
        ObservableCollection<Book> GiveBook(ObservableCollection<Book> books, string subscriberId, string bookInfo); 
        void GetBook(ObservableCollection<Book> books, string subscriberId, string bookId);
        ObservableCollection<LibSubscriber> AddSubscriber(LibSubscriber libSubscriber);
        ObservableCollection<LibSubscriber> RemoveSubscriber(LibSubscriber libSubscriber);

    }
}
