﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace MyLibTask
{
    [DataContract]
    public class LibSubscriber
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Surname { get; set; }
        [DataMember]
        public string Adress { get; set; }
        [DataMember]
        public List<string> TakenBooks { get; set; }

        public LibSubscriber() { }
        public LibSubscriber(string name, string surname, string adress)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Surname = surname;
            Adress = adress;
            TakenBooks = new List<string>();
         }
    }
}
