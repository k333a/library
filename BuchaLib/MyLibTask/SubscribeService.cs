﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace MyLibTask
{
    public class SubscribeService : ISubscribeService
    {
        ISearch _libSearch;
        IDbManager _dbManager;
        IArchiveManager _archivesManager;
        private ObservableCollection<LibSubscriber> _libSubscribers = new ObservableCollection<LibSubscriber>();
        string _DBName = "subscribers.xml";

        public SubscribeService(IDbManager dbManager, ISearch libSearch, IArchiveManager archivesManager)
        {
            _archivesManager = archivesManager;
            _libSearch = libSearch;
            _dbManager = dbManager;
             _libSubscribers = _dbManager.CheckDB(_DBName, _libSubscribers);
            _libSubscribers.CollectionChanged += SubscriberCollectionChanged;
        }

        private void SubscriberCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _dbManager.WriteToDb(_libSubscribers, _DBName);
        }

        public ObservableCollection<LibSubscriber> AddSubscriber(LibSubscriber libSubscriber)
        {
            _libSubscribers = _archivesManager.AddItem(libSubscriber, _libSubscribers);
            return _libSubscribers;
        }

        public ObservableCollection<LibSubscriber> RemoveSubscriber(LibSubscriber libSubscriber)
        {
            _libSubscribers = _archivesManager.RemoveItem(libSubscriber, _libSubscribers);
            return _libSubscribers;
        }

        public ObservableCollection<Book> GiveBook(ObservableCollection<Book> books, string subscriberId, string bookInfo)
        {
            ObservableCollection<Book> foundBooks = _libSearch.Find(books, bookInfo);
            ObservableCollection<Book> takenBook = null;
            ObservableCollection<LibSubscriber> libSubscribers = _libSearch.Find(_libSubscribers, subscriberId);

            if (foundBooks.Count != 0 && libSubscribers.Count != 0)
            {
                takenBook = _libSearch.Find(books, "True");
                libSubscribers[0].TakenBooks.Add(takenBook[0].Id.ToString());
                takenBook[0].Available = false;
                _dbManager.WriteToDb(_libSubscribers, _DBName);
            }
            return takenBook;
        }

        public void GetBook(ObservableCollection<Book> books, string subscriberId, string bookId)
        {
            ObservableCollection<LibSubscriber> libSubscribers = _libSearch.Find(_libSubscribers, subscriberId);
            ObservableCollection<Book> foundBooks = _libSearch.Find(books, bookId);

            if(foundBooks.Count != 0 && libSubscribers.Count != 0)
            {
                foundBooks[0].Available = true;
                libSubscribers[0].TakenBooks.Remove(bookId);
                _dbManager.WriteToDb(_libSubscribers, _DBName);
            }
        }

    }
}
