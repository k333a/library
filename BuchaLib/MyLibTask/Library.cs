﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace MyLibTask
{
    public class Library : ILibrary
    {
        public int CountBooks { get { return _library.Count; } }
        private ISearch _libSearch;
        private IDbManager _dbManager;
        private IArchiveManager _archivesManager;
        private ISubscribeService _subscribeService;
        private ObservableCollection<Book> _library;
        public string _DBName;

        public Library(IArchiveManager archivesManager, ISearch libSearch, IDbManager dbManager, string dBLibrary)
        {
            _library = new ObservableCollection<Book>();
            _dbManager = dbManager;
            _DBName = dBLibrary;
            _libSearch = libSearch;
            _archivesManager = archivesManager;
            _subscribeService = new SubscribeService(dbManager, libSearch, archivesManager);
            _library = _dbManager.CheckDB(_DBName, _library);
            _library.CollectionChanged += BookCollectionChanged;
        }

        private void BookCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _dbManager.WriteToDb(_library, _DBName);
        }

        public ObservableCollection<Book> AddBook(Book book)
        {
            _library = _archivesManager.AddItem<Book>(book, _library);
            return _library;
        }

        public ObservableCollection<Book> RemoveBook(Book book)
        {
            _library = _archivesManager.RemoveItem<Book>(book, _library);
            return _library;
        }

        public ObservableCollection<LibSubscriber> AddSubscriber(LibSubscriber libSubscriber)
        {
            return _subscribeService.AddSubscriber(libSubscriber);
        }

        public ObservableCollection<LibSubscriber> RemoveSubscriber(LibSubscriber libSubscriber)
        {
            return _subscribeService.RemoveSubscriber(libSubscriber);
        }

        public void DisplayAllBooks()
        {
            foreach (Book b in _library)
                Console.WriteLine(b);
        }

        public ObservableCollection<Book> Find(string data)
        {
            return _libSearch.Find(_library, data);
        }

        public ObservableCollection<Book> GiveBook(string subscriberId, string bookInfo)
        {
            ObservableCollection<Book> book = _subscribeService.GiveBook(_library, subscriberId, bookInfo);
            _dbManager.WriteToDb(_library, _DBName);
            return book;
        }

        public void GetBook(string subscriberId, string bookId)
        {
            _subscribeService.GetBook(_library, subscriberId, bookId);
            _dbManager.WriteToDb(_library, _DBName);
        }

    }
}
