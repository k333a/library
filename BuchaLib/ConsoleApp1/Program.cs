﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MyLibTask;

namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            Library BuchaLib = new Library(new ArchiveManager(), new LibSearch(), new DbManager(), "library.xml");
            BuchaLib.AddBook(new Book("Fight Club", "CP", 12, BookType.Art));
            BuchaLib.AddBook(new Book("ФФФ", "CP", 12, BookType.Art));
            BuchaLib.AddBook(new Book("Fight Club", "CP", 12, BookType.Art));
            BuchaLib.DisplayAllBooks();
            Console.WriteLine(BuchaLib.CountBooks);

            ObservableCollection<Book> found = BuchaLib.Find("Artts");
            foreach (Book b in found)
            {
                Console.WriteLine(b);
            }


            BuchaLib.AddSubscriber(new LibSubscriber("Ivan", "Ivanov", "Dom120"));
            BuchaLib.AddSubscriber(new LibSubscriber("Petr", "Ivanov", "Dom120"));
            BuchaLib.AddSubscriber(new LibSubscriber("Vasya", "Ivanov", "Dom120"));
            ObservableCollection<Book> findBook = BuchaLib.GiveBook("f7672d2e-58e2-47b8-bfd4-3df702e66712", "Fight Club");
 

            BuchaLib.GetBook("f7672d2e-58e2-47b8-bfd4-3df702e66712", "5099ff94-bf4a-4ef0-a2bb-1ed231327597");

            Console.ReadKey();
        }
    }
}
